document.querySelector('#SaveAspirant').addEventListener('click', saveAspirant);


function saveAspirant(){
    var sCi = document.querySelector('#ci').value,
        sName = document.querySelector('#name').value,
        slastName = document.querySelector('#lastName').value,
        syearBirth = document.querySelector('#yearBirth').value,
        sgender = document.querySelector('#gender').value,
        scopyCi = document.querySelector('#checkbox1').value,
        sbirthCertificate = document.querySelector('#checkbox2').value,
        sdegree = document.querySelector('#checkbox3').value,
        sgradesCard = document.querySelector('#checkbox4').value,
        sopsu = document.querySelector('#checkbox5').value,
        sblackBackground = document.querySelector('#checkbox6').value

        addAspirantToSistem(sCi, sName, slastName, syearBirth, sgender, scopyCi, sbirthCertificate, sdegree, sgradesCard, sopsu, sblackBackground); 
        drawAspirantTable();
}
function drawAspirantTable(){
    var list = getAspirantList(),
    tbody = document.querySelector('#table_student tbody');
    
tbody.innerHTML = '';

    for(var i = 0 ; i < list.length; i++){
        var row = tbody.insertRow(i),
            ciCell = row.insertCell(0),
            nameCell = row.insertCell(1),
            lastNameCell = row.insertCell(2),
            yearBirthCell = row.insertCell(3),
            genderCell = row.insertCell(4),
            copyCiCell = row.insertCell(5),
            birthCertificateCell = row.insertCell(6),
            degreeCell = row.insertCell(7),
            opsuCell = row.insertCell(8),
            blackBackgroundCell = row.insertCell(9);
            gradesCardCell = row.insertCell(10);
            

            ciCell.innerHTML = list[i].ci;
            nameCell.innerHTML = list[i].name;
            lastNameCell.innerHTML = list[i].lastName;
            yearBirthCell.innerHTML = list[i].yearBirth;
            genderCell.innerHTML = list[i].gender;
            copyCiCell.innerHTML = list[i].copyCi;
            birthCertificateCell.innerHTML = list[i].birthCertificate;
            degreeCell.innerHTML = list[i]. degree;
            opsuCell.innerHTML = list[i].opsu;
            blackBackgroundCell.innerHTML = list[i].blackBackground,
            gradesCardCell.innerHTML = list[i].gradesCard;


        tbody.appendChild(row);
    }
    
}