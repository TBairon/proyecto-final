<?php
	include_once 'MODELS/Candidate.php';
	include_once "SQL/InsertSQL.php";
	include_once "SQL/SelectSQL.php";
	include_once "SQL/UpdateSQL.php";	
	include_once "SQL/DeleteSQL.php";

	
	//$encode = json_encode($_POST);
	
	//$content = trim(file_get_contents("php://input"));
	//$decoded = json_decode($content, true);
	//print_r ($decoded);


	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	header('Content-Type: application/json');
	
	try {
		//Insert();
		Select();
		// Update();
		// Delete();
	} catch (Exception $e) {
		$return = array(
			'resultado' => $e->getMessage()
		);
		http_response_code(501);
		echo(json_encode($return));	
	}

	function Insert() {
		$candidate =  new Candidate('25253489', 'elion','Tablante', '1995-02-02', 'M');

		$insert = new InsertSQL();
		
		var_dump($insert->Insertar($candidate));

	}	

	function Select() {
		
		$select = new SelectSQL();
		$result = $select->Obtener(new Candidate());
		var_dump($result);
	}

	function Update(){
		$condiciones = array(
			array("Pais_Id" => array("Valor" => "2", "Condicion" => "=" ))
		);
		$colombia = new Pais('Colombia', 'America');
		$update = new UpdateSQL();
		$update->Actualizar($colombia, $condiciones);
	}

	function Delete(){
		$condiciones = array(
			array("Continente" => array("Valor" => "America", "Condicion" => "=" ))
		);
		$delete = new DeleteSQL();
		$delete->Eliminar(new Pais(), $condiciones);
	}
	//$aspirant =  new Aspirant(["ci"], ["name"],["lastName"], ["yearBirth"], ["gender"], ["copyCi"], ["birthCertificate"], 
							 // ["degree"], ["copyOpsu"], ["blackBackground"]);
	//$student = new Student($aspirant);
	
?>