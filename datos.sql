-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-11-2018 a las 02:58:28
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `datos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candidates`
--

CREATE TABLE `candidates` (
  `id` int(11) NOT NULL,
  `ci` int(10) NOT NULL,
  `name` varchar(25) COLLATE utf8mb4_spanish_ci NOT NULL,
  `lastName` varchar(25) COLLATE utf8mb4_spanish_ci NOT NULL,
  `yearBirth` date NOT NULL,
  `gender` varchar(5) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `candidates`
--

INSERT INTO `candidates` (`id`, `ci`, `name`, `lastName`, `yearBirth`, `gender`) VALUES
(1, 25253489, 'elion', 'Tablante', '1995-02-02', 'M'),
(2, 18545282, 'Armando', 'Rivero', '1994-08-24', 'M'),
(3, 25488793, 'Andrea', 'Gonzalez', '1996-08-21', 'F'),
(4, 24689413, 'Mariela', 'Reverol', '1995-06-23', 'F'),
(5, 24689397, 'Santiago', 'Andrade', '1993-10-12', 'M'),
(6, 1923598, 'Jesus', 'Valenzuela', '1992-11-10', 'M'),
(7, 26984653, 'Daniela', 'Guzman', '1994-12-13', 'F');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `candidates`
--
ALTER TABLE `candidates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
